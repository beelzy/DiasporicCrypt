
extends "res://scenes/common/Sensor.gd"

# sensor for Giant Lizard

var emeraldkeyclass = preload("res://scenes/items/special/emeraldkey.tscn")

var music
var bgm = preload("res://levels/common/boss1.ogg")

func _ready():
	music = get_tree().get_root().get_node("world/music")
	music.stop()
	gateclass = preload("res://scenes/fallislandcastle/gate.tscn")
	gatepos = Vector2(-208, -400)
	var boss = tilemap.get_node("BossGroup/GiantLizard")
	if (ProjectSettings.get("current_quest_complete")):
		boss.queue_free()
		if (!ProjectSettings.get("inventory").inventory.has("ITEM_EMERALDKEY")):
			var item = emeraldkeyclass.instance()
			item.set_global_position(Vector2(-608, -448))
			tilemap.get_node("BossGroup").add_child(item)

func trigger_fighting():
	music.set_stream(bgm)
	music.play()
	var boss = tilemap.get_node("BossGroup/GiantLizard")
	boss.get_node("roar").play()
	boss.animation_player.play("appear")
	boss.activate()

func process_fighting():
	if (!tilemap.get_node("BossGroup").has_node("GiantLizard")):
		gate.queue_free()
		set_physics_process(false)
