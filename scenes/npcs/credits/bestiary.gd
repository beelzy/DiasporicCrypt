extends Node2D

var bestiaryclass = preload("res://gui/menu/bestiary.tscn")

var music

func _ready():
	pass

func start(pos):
	var bestiaryinstance = bestiaryclass.instance()
	bestiaryinstance.set_position(Vector2(32, 20))
	#hide_dialog()
	get_tree().set_pause(true)
	var pause = get_tree().get_root().get_node("world/gui/CanvasLayer/pause")
	pause.get_node("menu").hide()
	pause.add_child(bestiaryinstance)
	pause.show()
