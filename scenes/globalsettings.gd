extends Node

const GLOBALSAVE = "global.save"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func save_globalsettings():
	var data = {}
	data.bgmvolume = ProjectSettings.get("bgmvolume")
	data.sfxvolume = ProjectSettings.get("sfxvolume")
	data.bgmmute = ProjectSettings.get("bgmmute")
	data.sfxmute = ProjectSettings.get("sfxmute")
	data.lights = ProjectSettings.get("lights")
	data.keyboard = ProjectSettings.get("keyboard_controls")
	data.gamepad = ProjectSettings.get("gamepad_controls")
	data.locale = TranslationServer.get_locale()
	data.achievements = ProjectSettings.get("achievements")
	data.ascrolls = ProjectSettings.get("ascrolls")
	data.fscrolls = ProjectSettings.get("fscrolls")
	data.anpcs = ProjectSettings.get("anpcs")
	data.fnpcs = ProjectSettings.get("fnpcs")
	data.aspells = ProjectSettings.get("aspells")
	data.fspells = ProjectSettings.get("fspells")
	data.adiscovery = ProjectSettings.get("adiscovery")
	data.fdiscovery = ProjectSettings.get("fdiscovery")
	data.alevels = ProjectSettings.get("alevels")
	data.flevels = ProjectSettings.get("flevels")
	data.abestiary = ProjectSettings.get("abestiary")
	data.fbestiary = ProjectSettings.get("fbestiary")
	data.paintings = ProjectSettings.get("paintings")
	var globaldir = ProjectSettings.get("globaldir")
	var dir = Directory.new()
	if (!dir.dir_exists(globaldir)):
		dir.make_dir(globaldir)
	var file = File.new()
	file.open(globaldir + "/" + GLOBALSAVE, File.WRITE)
	var test = to_json(data)
	file.store_string(to_json(data))
	file.close()
