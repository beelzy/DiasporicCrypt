extends Tween

var music

# Called when the node enters the scene tree for the first time.
func _ready():
	music = get_parent().get_node("music")

func fade_music(db = 0):
	interpolate_property(music, "volume_db", db, -80, 2.5, 1, Tween.EASE_IN, 0)
	start()

func _on_fade_tween_completed(object, key):
	object.stop()
	music.volume_db = 0
