extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var id = ""

var playerfound = false
var area

# Called when the node enters the scene tree for the first time.
func _ready():
	area = get_node("fake")
	set_physics_process(false)

func _physics_process(delta):
	var collisions = area.get_overlapping_bodies()
	for i in collisions:
		if (i.get_name() == "player"):
			playerfound = true
			painting_visible()
			i.get("area2d_blacklist").append(area)

func painting_visible():
	var paintings = ProjectSettings.get("paintings")
	if (!paintings.has(id)):
		paintings[id] = true
		ProjectSettings.set("paintings", paintings)
		get_node("/root/globalsettings").save_globalsettings()
		get_tree().get_root().get_node("world/gui/CanvasLayer/achievements/Achievement").checkPaintings()
	set_physics_process(false)

func _on_screen_entered():
	if (!playerfound):
		set_physics_process(true)

func _on_screen_exited():
	set_physics_process(false)
