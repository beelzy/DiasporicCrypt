extends Light2D

func _ready():
	get_tree().get_root().get_node("world/gui/CanvasLayer/pause/menu/panels/settings").connect("saved", self, "toggle_lights")
	get_tree().get_root().get_node("world").connect("loaded", self, "toggle_lights")
	toggle_lights()

func toggle_lights():
	visible = ProjectSettings.get("lights")
