extends "res://scenes/common/light.gd"

func toggle_lights():
	.toggle_lights()
	var animation_player = get_node("../AnimationPlayer")
	for animation_name in animation_player.get_animation_list():
		var animation = animation_player.get_animation(animation_name)
		animation.track_set_enabled(animation.find_track("Light2D:visible"), ProjectSettings.get("lights"))
