
extends "res://scenes/common/damagables/BaseEnemy.gd"

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	id = "priest"
	hp = 1
	gold = 100
	is_consumable = true
	consume_factor = 5
	consumable_size = Vector2(3, 1)
	consumable_sprite_offset = Vector2(-16, 0)

	current_hp = hp

func set_animation_direction(new_animation):
	.set_animation_direction(new_animation)
	get_node("glow_container").set_scale(Vector2(direction, 1))
