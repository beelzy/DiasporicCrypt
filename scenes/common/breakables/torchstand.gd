extends "res://scenes/common/breakables/torch.gd"

func toggle_lights():
	.toggle_lights()
	var break_animation = animation_player.get_animation("break")
	break_animation.track_set_enabled(break_animation.find_track("KinematicBody2D/Light2D:visible"), ProjectSettings.get("lights"))
