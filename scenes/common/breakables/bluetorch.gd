extends "res://scenes/common/breakables/torch.gd"

export var mode = "glow"

func _ready():
	current_animation = mode
	if ProjectSettings.get("lights"):
		get_node("AnimationPlayer").play(mode)

func _process(delta):
	if (is_crumbling && current_animation == mode):
		current_animation = "break"
	if ProjectSettings.get("lights"):
		update_animation()

func update_animation():
	if ProjectSettings.get("lights") || current_animation == "break":
		.update_animation()
