extends "res://scenes/common/breakables/pot.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().get_node("world/gui/CanvasLayer/pause/menu/panels/settings").connect("saved", self, "toggle_lights")
	get_tree().get_root().get_node("world").connect("loaded", self, "toggle_lights")
	toggle_lights()

func toggle_lights():
	if ProjectSettings.get("lights"):
		animation_player.play()
	else:
		animation_player.stop()
	get_node("KinematicBody2D/Light2D").visible = ProjectSettings.get("lights")
