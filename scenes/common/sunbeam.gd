extends "res://scenes/common/light.gd"

func _ready():
	get_node("../nolight").scale = scale

func toggle_lights():
	.toggle_lights()
	get_node("../nolight").visible = !ProjectSettings.get("lights")
