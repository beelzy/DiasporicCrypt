
extends Node

export var key = "ITEM_TOPAZKEY"

var keysprite
var aura
var collision
var detection

const keycolors = {
	"ITEM_RUBYKEY": Color(1, 0, 0),
	"ITEM_EMERALDKEY": Color(0, 1, 130/255.0),
	"ITEM_SAPPHIREKEY": Color(72/255.0, 178/255.0, 1),
	"ITEM_DIAMONDKEY": Color(1, 1, 1),
	"ITEM_TOPAZKEY": Color(1, 125/255.0, 0),
	"ITEM_PERIDOTKEY": Color(175/255.0, 1, 0),
	"ITEM_AMETHYSTKEY": Color(145/255.0, 0 , 1)
	}

func _ready():
	keysprite = get_node("key")
	collision = get_node("StaticBody2D")
	detection = get_node("Area2D")
	aura = get_node("gate").get_material().duplicate(true)
	var sprite = Image.new()
	sprite.load(ProjectSettings.get("itemfactory").items[key].image)
	var imageTexture = ImageTexture.new()
	imageTexture.create_from_image(sprite)
	keysprite.texture = imageTexture
	aura.set_shader_param("aura_color", keycolors[key])
	get_node("gate").set_material(aura)
	var node = NodePath("gate:material:shader_param/aura_width")
	var index = get_node("AnimationPlayer").get_animation("idle").find_track(NodePath("gate:material:shader_param/aura_width"))
	get_node("AnimationPlayer").get_animation("idle").track_set_path(index, node)
	set_physics_process(false)

func _physics_process(delta):
	if (collision.get_parent() != null):
		var collisions = detection.get_overlapping_bodies()
		for i in collisions:
			if (i.get_name() == "player" && ProjectSettings.get("inventory").inventory.has(key)):
				get_node("AnimationPlayer").play("open")
				remove_child(collision)

func enter_screen():
	set_physics_process(true)

func exit_screen():
	set_physics_process(false)

func _notification(what):
	if what == MainLoop.NOTIFICATION_PREDELETE:
		if is_instance_valid(collision) && collision.get_parent() == null:
			collision.free()
