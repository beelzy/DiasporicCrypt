extends AudioStreamPlayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var is_focused = true
var music_playing = true

var music

# Called when the node enters the scene tree for the first time.
func _ready():
	music = get_tree().get_root().get_node("world/music")
	music_playing = music.playing
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_FOCUS_IN && !is_focused):
		if (music_playing):
			play(get_playback_position())
		is_focused = true
	elif (what == MainLoop.NOTIFICATION_WM_FOCUS_OUT && is_focused):
		music_playing = playing
		stop()
		is_focused = false
