extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().get_node("world/gui/CanvasLayer/pause/menu/panels/settings").connect("saved", self, "toggle_lights")
	get_tree().get_root().get_node("world").connect("loaded", self, "toggle_lights")
	toggle_lights()

func toggle_lights():
	var lights = ProjectSettings.get("lights")
	var animation_player = get_node("AnimationPlayer")
	if lights:
		animation_player.play()
	else:
		animation_player.stop()
	get_node("Light2D").visible = lights
	get_node("Light2D2").visible = lights
	get_node("Light2D3").visible = lights
