# Character Credits

- [Pepper and Carrot](https://peppercarrot.com) - David Revoy, CC-By
- [Goddess](https://github.com/WinterLicht/Chaos-Projectile) - Anna Dorokhova
- Nystev - Carbonoid
- Taevica - Carbonoid
- [Neropheus](http://aishishii.tumblr.com/tagged/neropheus) - Aishishii
- Yuki - MissHolic
- Leopold - Slayernice
- [Hatherton](https://n-jay81.tumblr.com/tagged/hatherton) - N-Jay
- Aethea - kirakiraprince
- Diato - unafkenny
- Jaw - unafkenny

# Attributions

## Graphics
Derivative (crop) of [char.png](https://raw.githubusercontent.com/WinterLicht/Chaos-Projectile/master/src/data/char.png) by Anna Dorokhova, CC-BY 4.0

## Audio
- [air_cut.wav](https://freesound.org/people/Langerium/sounds/84616/) by Langerium, CC0
- [woosh.wav](https://freesound.org/people/Intimidated/sounds/74234/) by Intimidated, CC-BY 3.0
- [Bloody Blade 2.wav](https://freesound.org/people/Kreastricon62/sounds/323526/) by Kreastricon62, CC0
- [Sword Fight Hat.aif](https://freesound.org/people/Walter_Odington/sounds/25704/) by Walter_Odington, CC0
- [Bloody Blade.wav](https://freesound.org/people/Kreastricon62/sounds/323525/) by Kreastricon62, CC0
- [Unsheathed blade](https://freesound.org/people/jamesabdulrahman/sounds/320521/) by jamesabdulrahman, CC0
- [Buffer Spell](https://www.freesound.org/people/Northern_Monkey/sounds/176741/) by Northern_Monkey, CC0
- [tremor.wav](https://freesound.org/people/Marec/sounds/19994/) by Marec, CC-BY 3.0
- Fire_Burning-JaBa-810606813.wav from SoundBible.com by JaBa, CC-BY 3.0
- Small Fireball-SoundBible.com-1381880822.wav from SoundBible.com by Mike Koenig, CC-BY 3.0
- curse.ogg, warp.ogg, zap2b.ogg and zap5a.ogg in [Spell Sounds Starter Pack](https://opengameart.org/content/spell-sounds-starter-pack) by p0ss, CC-BY-SA 3.0
- Ice attack 2.wav, Healing Full.wav and Misc 02.wav in [Magic SFX Sample](https://opengameart.org/content/magic-sfx-sample) by ViRiX, CC-BY 3.0
- Item2A.wav in [UI and Item Sound Effect Jingles Sample 2](https://opengameart.org/content/ui-and-item-sound-effect-jingles-sample-2) by ViRiX, CC-BY 3.0
- Bomb-SoundBible.com-891110113.wav from SoundBible.com by Mike Koenig, CC-BY 3.0
- DM-CGS-38.wav, DM-CGS-39.wav, DM-CGS-40.wav, DM-CGS-44.wav, and DM-CGS-47.wav in [Casual Game Sounds - Single Shot SFX Pack](http://dustyroom.com/casual-game-sfx-freebie/) by Dustyroom, CC0
- [Electric](https://freesound.org/people/Bekir_VirtualDJ/sounds/132834/) by Bekir_VirtualDJ, CC0
- [Wind Whirr](https://freesound.org/people/Autistic Lucario/sounds/140054/) by Autistic Lucario, CC-BY 3.0
- [DRAGON_ROAR.wav](https://freesound.org/people/JoelAudio/sounds/85568/) by JoelAudio, CC0
- load.wav in [GUI Sound Effects](https://opengameart.org/content/gui-sound-effects) by LokiF, CC0
- [strong wind blowing.mp3](https://opengameart.org/content/strong-wind-blowing) by DontMind8, CC-BY 4.0
- [weapon.wav](https://freesound.org/people/RunnerPack/sounds/87046/) by RunnerPack, CC-BY 3.0
- paino.wav in [11 male human pain/death sounds](https://opengameart.org/content/11-male-human-paindeath-sounds) by Michel Baradari, CC-BY 3.0
- [woman fighting.wav](https://freesound.org/people/drotzruhn/sounds/405328/) by drotzruhn, CC-BY 3.0
- [Boot foley](https://freesound.org/people/Slave2theLight/sounds/157023/) by Slave2theLight, CC-BY 3.0
- [DenseCrunchRe-rated_CaveIn.wav](https://freesound.org/people/zimbot/sounds/244484/) by zimbot, CC-BY 3.0 
- space shield sounds - 7.wav and space shield sounds - 4.wav in [Space Ship Shield Sounds](https://opengameart.org/content/space-ship-shield-sounds) by bart, CC0
- [thunder.wav](https://freesound.org/people/SGAK/sounds/467777/) by SGAK, CC0
- [Splat_09.wav](https://freesound.org/people/LittleRobotSoundFactory/sounds/316542/) by Little Robot Sound Factory, CC-BY 3.0
- [Explosion2.wav](https://freesound.org/people/steveygos93/sounds/80401/) by steveygos93, CC-BY 3.0
- [Skeleton bones (game).wav](https://freesound.org/people/Cribbler/sounds/381859/) by Cribbler, CC0
- [Water Swirl, Small, 3.wav](https://freesound.org/people/InspectorJ/sounds/398723/) by InspectorJ, CC-BY 3.0
- [Splash, Jumping, G.wav](https://freesound.org/people/InspectorJ/sounds/352103/) by InspectorJ, CC-BY 3.0
- [hanging.wav](https://freesound.org/people/jameswrowles/sounds/274138/) by jameswrowles, CC0
- [WHIP.flac](https://freesound.org/people/qubodup/sounds/182577/) by qubodup, CC0
- swosh-17.wav and swosh-24.wav in [Swish - bamboo stick weapon swhoshes](https://opengameart.org/content/swish-bamboo-stick-weapon-swhoshes) by Iwan Gabovitch, CC0
- [slashkut.wav](https://freesound.org/people/Abyssmal/sounds/35213/) by Abyssmal, CC0
- Socapex - big punch.wav and Socapex - small knock.wav in [Punches, hits, swords and squishes](https://opengameart.org/content/punches-hits-swords-and-squishes) by Socapex, CC-BY-SA 3.0
- [Break something (ice/glass/...)](https://freesound.org/people/Aurelon/sounds/422633/) by Aurelon, CC0
- Children Spirit Sounds.wav and Pixie.wav in [Monster or beast sounds](https://opengameart.org/content/monster-or-beast-sounds) by pauliuw, CC0
- [ghost.wav](https://opengameart.org/content/ghost-pain) by Vinrax, CC-BY 3.0
- [metal sheet drop on pile.wav](https://freesound.org/people/kyles/sounds/453349/) by kyles, CC0
- [54.wav](https://freesound.org/people/JUDITH136/sounds/408012/) by JUDITH136, CC-BY 3.0
- [Grunt2 - Death Pain.wav](https://freesound.org/people/AlineAudio/sounds/416838/) by AlineAudio, CC0
- [Male_Death_04](https://freesound.org/people/Artmasterrich/sounds/345456/) by Artmasterrich, CC0
- [Zombie (Or Monster Or Lion) Roar](https://freesound.org/people/zglar/sounds/232289/) by zglar, CC-BY 3.0
- monster-4.wav in [Monster Sound Effects Pack](https://opengameart.org/content/monster-sound-effects-pack) by Ogrebane, CC0
- [Osare Minotaur Sounds](https://opengameart.org/content/osare-minotaur-sounds) by Brandon Morris, CC0
- painr.wav in [15 monster grunt/paint/death sounds](https://opengameart.org/content/15-monster-gruntpaindeath-sounds) by Michel Baradari, CC-BY 3.0
- [hero_death.wav](https://freesound.org/people/rdaly95/sounds/387161/) by rdaly95, CC-BY 3.0
- [howl.mp3](https://freesound.org/people/Taira Komori/sounds/213140/) by Taira Komori, CC-BY 3.0
- [On Off Switch](https://freesound.org/people/velosiped/sounds/263802/) by velosiped, CC0
- [magic_bell_teleport_synth.wav](https://freesound.org/people/wangzhuokun/sounds/439528/) by wangzhuokun, CC-BY 3.0
- [Door_Open.wav](https://freesound.org/people/Tabook/sounds/400329/) by Tabook, CC0
- [Magic mallet.wav](https://freesound.org/people/Hotlavaman/sounds/349201/) by Hotlavaman, CC0
- [spooky gong.wav](https://freesound.org/people/Veiler/sounds/207167/) by Veiler, CC0
- Thump in [Bangs and Booms](https://freesound.org/people/bareform/sounds/218717/) by bareform, CC-BY 3.0
- Dinosaur_Loud Roar2.mp3 in [The 101 SFX Collection](https://freesound.org/people/CGEffex/sounds/89550/) by CGEffex, CC-BY 3.0
- [splat2.ogg](url=https://freesound.org/people/gprosser/sounds/361030/) by gprosser, CC0
- Space Laser - Sci-Fi Sound Effects .wav in [Out Of This World Futuristic Sci-Fi Sound Effects](https://freesound.org/people/BurghRecords/sounds/408243/) by BurghRecords, CC0
- Spell_00.wav in [Fantasy Sound Effects Library](https://opengameart.org/content/fantasy-sound-effects-library) by Little Robot Sound Factory, CC-BY 3.0
- [Dragon roar](https://freesound.org/people/veroma/sounds/169055/) by veroma, CC-BY 3.0
- [Fireball Explosion.wav](https://freesound.org/people/HighPixel/sounds/431174/) by HighPixel, CC0
- Witch Cackle in [Creature Sounds](https://freesound.org/people/AntumDeluge/sounds/417826/) by AntumDeluge, CC0
- [cig_extinguish.wav](https://freesound.org/people/the_semen_incident/sounds/155660/) by the_semen_incident, CC0
- BGM1.ogg, BGM2.ogg, BGM4.ogg by Hubert Lamontagne, CC-BY-SA 4.0
- global.ogg, catacombs.ogg, BGM5.ogg, boss1.ogg, boss2a.ogg, boss2f.ogg, soprano.ogg, alto.ogg, tenor.ogg, bass.ogg, boss2f-full.ogg, boss3.ogg, colosseum.ogg, BGM1.ogg (Forest), BGM2.ogg (Forest), BGM3.ogg (Forest), healing.wav by Stephen Cromwell, CC-BY 4.0
