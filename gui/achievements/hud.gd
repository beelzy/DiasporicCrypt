extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func displayAchievement(id):
	get_node("icon").set_texture(ProjectSettings.get("achievement_icons")[id])
	get_node("title").set_text(tr(id))
	get_node("AnimationPlayer").play("unlock")
	var achievements = ProjectSettings.get("achievements")
	achievements.append(id)
	ProjectSettings.set("achievements", achievements)
	get_node("/root/globalsettings").save_globalsettings()

func checkGold():
	if (ProjectSettings.get("gold") >= 1000000 && !ProjectSettings.get("achievements").has("ACH_BANKER")):
		displayAchievement("ACH_BANKER")

func checkScrolls():
	var scrolls = "fscrolls"
	if (ProjectSettings.get("player") == "adela"):
		scrolls = "ascrolls"
	if (ProjectSettings.get("scrolls").size() == 8):
		ProjectSettings.set(scrolls, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("fscrolls") && ProjectSettings.get("ascrolls") && !ProjectSettings.get("achievements").has("ACH_ACADEMIC")):
		displayAchievement("ACH_ACADEMIC")

func checkNPCs():
	var player = ProjectSettings.get("player")
	var npctotal = 8
	var npcs = "fnpcs"
	if (player == "adela"):
		npctotal = 14
		npcs = "anpcs"
	if (ProjectSettings.get("npcs_found").size() == npctotal):
		ProjectSettings.set(npcs, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("fnpcs") && ProjectSettings.get("anpcs") && !ProjectSettings.get("achievements").has("ACH_FRIEND")):
		displayAchievement("ACH_FRIEND")

func checkSpells():
	var spells = "fspells"
	if (ProjectSettings.get("player") == "adela"):
		spells = "aspells"
	if (ProjectSettings.get("available_spells").size() == 7):
		ProjectSettings.set(spells, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("fspells") && ProjectSettings.get("aspells") && !ProjectSettings.get("achievements").has("ACH_WIZARD")):
		displayAchievement("ACH_WIZARD")

func checkDiscovery():
	var discovery = "fdiscovery"
	var total = "fdiscoverytotal"
	var levels = ProjectSettings.get("DISCOVERY_FRIEDERICH")
	if (ProjectSettings.get("player") == "adela"):
		discovery = "adiscovery"
		total = "adiscoverytotal"
		levels = ProjectSettings.get("DISCOVERY_ADELA")
	var size = levels.size()
	var discovery_total = 0
	for i in range(0, size):
		discovery_total += ProjectSettings.get("levels").levels[levels[i]].location.discovered_tiles
	if (discovery_total >= ProjectSettings.get(total)):
		ProjectSettings.set(discovery, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("fdiscovery") && ProjectSettings.get("adiscovery") && !ProjectSettings.get("achievements").has("ACH_EXPLORER")):
		displayAchievement("ACH_EXPLORER")

func checkLevels():
	var completed_levels = 0
	var levels = "flevels"
	if (ProjectSettings.get("player") == "adela"):
		levels = "alevels"
	for level in ProjectSettings.get("levels").levels:
		if (ProjectSettings.get("levels").levels[level].complete):
			completed_levels += 1
	if (completed_levels == ProjectSettings.get("QUEST_TOTAL")):
		ProjectSettings.set(levels, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("flevels") && ProjectSettings.get("alevels") && !ProjectSettings.get("achievements").has("ACH_COMPLETIONIST")):
		displayAchievement("ACH_COMPLETIONIST")

func checkBestiary():
	var discovered = 0
	var bestiary = "fbestiary"
	var total = ProjectSettings.get("fbestiarytotal")
	if (ProjectSettings.get("player") == "adela"):
		bestiary = "abestiary"
		total = ProjectSettings.get("abestiarytotal")
	for item in ProjectSettings.get("bestiary_discovered"):
		if (ProjectSettings.get("bestiary_discovered")[item] > 0):
			discovered += 1
	if (discovered >= total):
		ProjectSettings.set(bestiary, true)
		get_node("/root/globalsettings").save_globalsettings()
	if (ProjectSettings.get("fbestiary") && ProjectSettings.get("abestiary") && !ProjectSettings.get("achievements").has("ACH_HUNTER")):
		displayAchievement("ACH_HUNTER")

func checkPaintings():
	if (ProjectSettings.get("paintings").size() == 4 && !ProjectSettings.get("achievements").has("ACH_CURATOR")):
		displayAchievement("ACH_CURATOR")
