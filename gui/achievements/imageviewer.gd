extends Control

var viewport
var container
var overlay
var next
var previous

var slides = {
	"ACH_SPARKLEPIRE": [
		preload("res://gui/achievements/sparklepire/0.tscn")
	],
	"ACH_CURATOR": [
		preload("res://gui/achievements/curator/0.tscn")
	],
	"ACH_COMPLETIONIST": [
		preload("res://gui/achievements/completionist/0.tscn"),
		preload("res://gui/achievements/completionist/1.tscn"),
		preload("res://gui/achievements/completionist/2.tscn")
	],
	"ACH_EXPLORER": [
		preload("res://gui/achievements/explorer/0.tscn"),
		preload("res://gui/achievements/explorer/1.tscn")
	],
	"ACH_HUNTER": [
		preload("res://gui/achievements/hunter/0.tscn"),
		preload("res://gui/achievements/hunter/1.tscn"),
		preload("res://gui/achievements/hunter/2.tscn")
	],
	"ACH_FRIEND": [
		preload("res://gui/achievements/friend/0.tscn")
	],
	"ACH_ACADEMIC": [
		preload("res://gui/achievements/academic/0.tscn"),
		preload("res://gui/achievements/academic/1.tscn"),
		preload("res://gui/achievements/academic/2.tscn")
	],
	"ACH_WIZARD": [
		preload("res://gui/achievements/wizard/0.tscn"),
		preload("res://gui/achievements/wizard/1.tscn")
	],
	"ACH_LEVELGRINDER": [
		preload("res://gui/achievements/levelgrinder/0.tscn"),
		preload("res://gui/achievements/levelgrinder/1.tscn")
	],
	"ACH_BANKER": [
		preload("res://gui/achievements/banker/0.tscn")
	]
}
var index = 0
var id

# Called when the node enters the scene tree for the first time.
func _ready():
	viewport = get_node("viewport")
	container = get_node("viewport/contents")
	overlay = get_node("overlay")
	next = get_node("next")
	previous = get_node("previous")
	var hide = get_node("overlay/help/show")
	hide.get_node("input").set_text(tr("ACH_SHOWHIDE"))
	hide.set_key("ui_select")
	set_process(false)
	set_process_input(false)

func prepare_slides(newid):
	id = newid
	index = 0
	if (slides[id].size() > 0):
		load_slide()
	previous.hide()
	if (slides[id].size() < 2):
		next.hide()
	else:
		next.show()

func load_slide():
	viewport.remove_child(container)
	container.queue_free()
	viewport.add_child(slides[id][index].instance())
	container = get_node("viewport/contents")
	overlay.get_node("caption").set_text(tr(id + "_CAPTION" + str(index)))

func reload_keys():
	get_node("overlay/help/show").reload_key()
	get_node("overlay/help/scroll").reload_key()

func _input(event):
	if (event.is_pressed() && !event.is_echo()):
		if (event.is_action_pressed("ui_select")):
			overlay.set_visible(!overlay.visible)
		if (previous.visible && event.is_action_pressed("ui_left")):
			index -= 1;
			load_slide()
		if (next.visible && event.is_action_pressed("ui_right")):
			index += 1;
			load_slide()
		previous.hide()
		next.hide()
		if (index > 0):
			previous.show()
		if (index < slides[id].size() - 1):
			next.show()

func _process(delta):
	if (Input.is_action_pressed("ui_down")):
		var posY = max(container.get_position().y - 3, 336 - container.rect_size.y)
		container.set_position(Vector2(0, posY))
	elif (Input.is_action_pressed("ui_up")):
		var posY = min(container.get_position().y + 3, 0)
		container.set_position(Vector2(0, posY))

func _on_focus_entered():
	set_process(true)
	set_process_input(true)

func _on_focus_exited():
	overlay.set_visible(true)
	set_process(false)
	set_process_input(false)
