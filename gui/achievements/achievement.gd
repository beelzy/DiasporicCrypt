extends "res://gui/dialogue/choice.gd"

export var id = "ACH_SPARKLEPIRE"

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("title").set_text(tr(id))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
