extends Control

var list
var title
var view
var stats
var weaknesses
var resistances
var selectedindex = 0

var scrollrange

var bestiary
var discovered
var listitemclass = preload("res://gui/menu/listitem.tscn")

var elementalicons = {
	"fire": preload("res://players/magic/fire/icon.png"),
	"ice": preload("res://players/magic/ice/icon.png"),
	"thunder": preload("res://players/magic/thunder/icon.png"),
	"dark": preload("res://players/magic/hex/icon.png"),
	"magicmine": preload("res://players/magic/magicmine/icon.png"),
	"wind": preload("res://players/magic/wind/icon.png"),
	"earth": preload("res://players/magic/earth/icon.png")
}

var locations = {}

func _ready():
	scrollrange = get_node("column/list").get_size()
	var back = get_node("back")
	back.get_node("input").set_text(tr("MAP_BACK"))
	back.set_key("ui_cancel")
	discovered = ProjectSettings.get("bestiary_discovered")
	bestiary = ProjectSettings.get("bestiary")
	title = get_node("column/name")
	view = get_node("column/view")
	stats = get_node("column2/stats")
	weaknesses = get_node("column2/weaknesses")
	resistances = get_node("column2/resistances")
	list = get_node("column/list/container")

	var available_levels = ProjectSettings.get("available_levels")
	var seen = discovered.keys().size()
	var defeated = 0
	for item in discovered:
		if (discovered[item] > 0):
			defeated += 1
	get_node("counter").set_text(tr("KEY_SEEN") + ": " + str(seen) + " " + tr("KEY_DEFEATED") + ": " + str(defeated))

	if (discovered.keys().size() == 0):
		set_process_input(false)
		title.hide()
		stats.hide()
		get_node("column2/weak").hide()
		get_node("column2/resist").hide()
	else:
		set_process_input(true)
		get_node("column/empty").hide()
		for id in discovered.keys():
			var item = bestiary[id]
			var listitem = listitemclass.instance()
			var title = "????"
			if (discovered[id] > 0):
				title = tr(item.name)
			listitem.get_node("title").set_text(title)
			listitem.set_name(id)
			listitem.set_focus_neighbour(MARGIN_LEFT, ".")
			listitem.set_focus_neighbour(MARGIN_RIGHT, ".")
			listitem.set_focus_neighbour(MARGIN_BOTTOM, ".")
			listitem.connect("focus_entered", self, "check_scroll")
			# previous item should focus the next item properly
			if (list.get_child_count() > 0):
				var lastitem = list.get_child(list.get_child_count() - 1)
				lastitem.set_focus_neighbour(MARGIN_BOTTOM, "")
			list.add_child(listitem)
			locations[id] = []
			for location in item.locations:
				if (available_levels.has(location)):
					locations[id].append(tr(location))
		if (list.get_child_count() > 0):
			list.get_child(0).set_focus_neighbour(MARGIN_TOP, ".")
		list.get_child(0).grab_focus()
		displayEnemy()

func displayEnemy():
	var id = list.get_child(selectedindex).get_name()
	var item = bestiary[id]
	var enemy = item.class.instance()
	if (item.level > 1):
		enemy.set("level", item.level)
	var old_enemy = view.get_child(0)
	view.remove_child(old_enemy)
	if is_instance_valid(old_enemy):
		old_enemy.queue_free()
	view.add_child(enemy)
	if (enemy.has_node("enabler")):
		var enabler = enemy.get_node("enabler")
		enemy.remove_child(enabler)
		enabler.queue_free()
	enemy.set_physics_process(false)

	for weakness in weaknesses.get_children():
		weaknesses.remove_child(weakness)
		weakness.queue_free()
	for resistance in resistances.get_children():
		resistances.remove_child(resistance)
		resistance.queue_free()

	if (discovered[id] > 0):
		var consumable = ""
		var sunbeam = ""
		var magic = ""
		if (enemy.get("is_consumable")):
			consumable = "[img]res://gui/bestiary/consumable.png[/img]"
		if (!enemy.get("sunbeam_immunity")):
			sunbeam = "[img]res://gui/bestiary/sunbeam.png[/img]"
		if (enemy.get("magic_only")):
			magic = "[img]res://gui/bestiary/magic.png[/img]"
		title.set_bbcode("[center]" + consumable + sunbeam + magic + " " + tr(item.name) + "[/center]")
		var hp = str(enemy.get("hp")) + tr("STATS_HP")
		var ep = str(enemy.get("ep")) + tr("STATS_EXP")
		var gold = str(enemy.get("gold")) + "G"
		var atk = tr("STATS_ATK") + ": " + str(enemy.get("atk"))
		var def = tr("STATS_DEF") + ": " + str(enemy.get("def"))
		var location = tr("KEY_LOCATION") + ": " + PoolStringArray(locations[id]).join(", ")
		var defeated = tr("KEY_DEFEATED") + ": " + str(discovered[id])
		stats.set_text(hp + "\n\n" + ep + "\n\n" + gold + "\n\n" + atk + "\n\n" + def + "\n\n" + location + "\n\n" + defeated)
		var enemyweaknesses = enemy.get("elemental_weaknesses")
		if (enemyweaknesses.size() > 0 && enemyweaknesses[0] == "all"):
			enemyweaknesses = elementalicons.keys()
		for weakness in enemyweaknesses:
			var icon = TextureRect.new()
			icon.set_texture(elementalicons[weakness])
			weaknesses.add_child(icon)
		var enemyresistances = enemy.get("elemental_protection")
		if (enemyresistances.size() > 0 && enemyresistances[0] == "all"):
			enemyresistances = elementalicons.keys()
		for resistance in enemyresistances:
			var icon = TextureRect.new()
			icon.set_texture(elementalicons[resistance])
			resistances.add_child(icon)
	else:
		title.set_bbcode("[center]????[/center]")
		var hp = "????" + tr("STATS_HP")
		var ep = "????" + tr("STATS_EXP")
		var gold = "????" + "G"
		var atk = tr("STATS_ATK") + ": " + "???"
		var def = tr("STATS_DEF") + ": " + "???"
		var location = tr("KEY_LOCATION") + ": ????"
		var defeated = tr("KEY_DEFEATED") + ": ????"
		stats.set_text(hp + "\n\n" + ep + "\n\n" + gold + "\n\n" + atk + "\n\n" + def + "\n\n" + location + "\n\n" + defeated)

func check_scroll():
	var item = get_focus_owner()
	var vscroll = get_node("column/list").get_v_scroll()
	var itempos = item.get_position().y
	var itemsize = item.get_size().y
	if (vscroll > itempos || vscroll + scrollrange.y < itempos + itemsize):
		get_node("column/list").set_v_scroll(itempos)

func _input(event):
	if (event.is_pressed() && !event.is_echo()):
		var focus = get_focus_owner()
		if (event.is_action_pressed("ui_up")):
			selectedindex = max(0, selectedindex - 1)
			displayEnemy()
		elif (event.is_action_pressed("ui_down")):
			selectedindex = min(selectedindex + 1, list.get_child_count() - 1)
			displayEnemy()

func block_cancel():
	return false
